# GSoC 2020 - SWAN-Rucio integration tasks

There are my solution for the exercises for candidate students to the SWAN-Rucio integration
project.

## How to run

1. Clone the repository, and ensure to download all the submodules

```
git clone --recursive git@gitlab.com:zodiacfireworks/gsco-2020-swan-rucio-integration.git
```

2. Install the dependencies with poetry

```
poetry install
```

or just with pip

```
python -m pip install -r requirements.txt
```

3. Launch jupyter notebook and check the tasks, with poetry

```
poetry run jupyter lab
```

or just

```
jupyter lab
```

## Tasks specifications

### Task 1

The task 1 contains only a notebook with the solution of the excercie specification

![Task 1](./task-1.png 'Task 1')

### Tasks 2 and 3

This tasks have been bundled as npm package for task 2 and pypi package for task 3, both works
together with the folowing commands with poetry

```
poetry run jupyter labextension install weatherlab
poetry run python -m pip install weatherlab
```

or just with pip

```
jupyter labextension install weatherlab
python -m pip install weatherlab
```

The weatherlab npm package can also be instaled from the extension manage. After install just
reload jupyter lab and go to `task3/examples/Example.ipynb` to se the example.

Packages are

- Taks 2 (npm:weatherlab)

  - NPM: https://www.npmjs.com/package/weatherlab
  - Gitlab: https://gitlab.com/zodiacfireworks/weatherlab

- Task 3 (pypi:weatherlab)
  - PyPi: https://pypi.org/project/weatherlab
  - Gitlab: https://gitlab.com/zodiacfireworks/weatherlab-extension

Each package have its own documentation.

The weather data is provides by [Open Weather Map](https://openweathermap.org/)

![Task 2 and 3](./task-2-3.png 'Task 2 and 3')
